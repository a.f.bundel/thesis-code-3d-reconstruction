# Thesis code
This repository contains the main codebase for my Master's thesis project on depth estimation from monocular video, conducted as part of a graduation internship at Fugro. The provided scripts create training data for 3D object detection: MMDetection3D and Gen-LaneNet. 

# Setup and installation
Install Python dependencies:

```
pip install -r requirements.txt
```

Either directly or within a [conda](https://docs.conda.io/en/latest/) environment.

## Data folder structure
As provided by Fugro, the following folder structure for the data is assumed:

```
data
├── ground-truth            # Contains 3D annotated LiDAR data
├── LAZ                     # LiDAR point clouds
│   ├── 2019-04-30_b_10        
│   └── 2019-04-30_b_9    
├── ShapeFiles              
│   ├── LAZ        
│   ├── Scope      
│   └── Video           
└── Video                   # Video data
    ├── Calibration         
    ├── ep11-201002303-20190430-075921  # Scan run 1
    │   └── cam2                        # Center camera frames
    ├── ep11-201002303-20190430-080329  # Scan run 2
    │   └── cam2                        
    ├── ep11-201002303-20190430-080712  # Scan run 3
    │   └── cam2
    └── externalOrientation
        ├── ep11-201002303-20190430-075921  # Scan run 1 camera GPS coordinates
        ├── ep11-201002303-20190430-080329      
        └── ep11-201002303-20190430-080712 

```

# Usage
Run from main.py with:

```
usage: main.py [-h] [--cams {cam1,cam2,cam3} [{cam1,cam2,cam3} ...]]
               [--runs {ep11-201002303-20190430-075921,ep11-201002303-20190430-080329,ep11-201002303-20190430-080712} [{ep11-201002303-20190430-075921,ep11-201002303-20190430-080329,ep11-201002303-20190430-080712} ...]]
               [--overlay_data | --create_dwg_bboxes | --create_dwg_tracks | --create_training_dataset_bboxes | --create_training_dataset_tracks | --project_dwg_on_camera | --visualize_kitti_bboxes]

Overlay LiDAR with camera data or create training data for 3D object detection

optional arguments:
  -h, --help            show this help message and exit
  --cams {cam1,cam2,cam3} [{cam1,cam2,cam3} ...]
                        specify which cameras to include (default is middle camera)
  --runs {ep11-201002303-20190430-075921,ep11-201002303-20190430-080329,ep11-201002303-20190430-080712} [{ep11-201002303-20190430-075921,ep11-201002303-20190430-080329,ep11-201002303-20190430-080712} ...]
                        specify which scanning runs to include (default is 1st)
  --overlay_data        overlay camera and LiDAR data from specified run(s) and camera(s)
  --create_dwg_bboxes   Create bounding boxes DWG file
  --create_dwg_tracks   Create track lines DWG file
  --create_training_dataset_bboxes
                        Create dataset for SMOKE (3D object detection) in KITTI format from bounding boxes
  --create_training_dataset_tracks
                        Create dataset for Gen-LaneNet (3D object detection) in Apollo format from track lines
  --project_dwg_on_camera
                        Visualize annotations from DWG format
  --visualize_kitti_bboxes
                        Visualize bounding boxes from KITTI format
```

## Demo
The provided notebooks were created to run in Google Colab, but can be edited 
to run locally.