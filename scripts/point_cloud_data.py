import laspy
import numpy as np
import open3d
import matplotlib.pyplot as plt
import subprocess

from config import config
from scripts import utils


def get_data(path):
    file = laspy.open(path, mode='r')
    return laspy.convert(file.read(), 
                         file_version=config.LASPY_FILE_VERSION, 
                         point_format_id=config.LASPY_POINT_FORMAT)


def show_basic_info(las):
    print('LAS version:', las.header.version)
    ground_pts = las.classification == 1
    bins, counts = np.unique(las.return_number[ground_pts], return_counts=True)
    print('Ground Point Return Number distribution:')
    for r, c in zip(bins, counts):
        print(f'    {r}:{c}')

    print('Data time:', utils.gps_to_utc(las.gps_time[0]))  # 1240646703.3269112 -> 2019-04-30 08:05:12.326911
    print('Dimensions:')
    for dimension in las.point_format.dimensions:
        print(dimension.name, end=', ')
    print()
    print('Points from Header:', las.header.point_count)


def open_large_file(file):
    for points in file.chunk_iterator(1_000_000):
        print(points) 


def append_las_to_array(points_acc, las_points):
    # As used by e.g. KITTI format
    # From https://github.com/PRBonn/lidar-bonnetal/issues/78
    x = las_points.X
    y = las_points.Y
    z = las_points.Z
    i = las_points.intensity
    arr = np.zeros(x.shape[0] + y.shape[0] + z.shape[0] + i.shape[0], dtype=np.float32)
    arr[::4] = x
    arr[1::4] = y
    arr[2::4] = z
    arr[3::4] = i
    points_acc = np.append(points_acc, arr)
    return points_acc


def write_to_las(pcd, output_path, filename):
    # From https://laspy.readthedocs.io/en/latest/examples.html#creating-from-scratch
    las = laspy.create(point_format=config.LASPY_POINT_FORMAT, 
                       file_version=config.LASPY_FILE_VERSION)
    las.x = pcd[:,0] 
    las.y = pcd[:,1]
    las.z = pcd[:,2]
    las.write(f'{output_path}{filename}.las')


def create_open3d_pc(lidar, cam_image=None):
    # create open3d point cloud
    pcd = open3d.geometry.PointCloud()
    # assign point coordinates
    pcd.points = open3d.utility.Vector3dVector(lidar)
    return pcd


def convert_lidars_to_open3d(lidars):
    # Create one Open3D point cloud from a (list of) lidars
    pcd = open3d.geometry.PointCloud()
    pcd_points = []
    for lidar in lidars:
        # Shape to (num_points, 3)
        points = np.asarray([lidar.x, lidar.y, lidar.z]).transpose()
        pcd_points.extend(points)
    pcd.points = open3d.utility.Vector3dVector(np.asarray(pcd_points))

    return pcd


def classify_ground(data_path, filename):
    # Create one classified point cloud from a (list of) lidar
    exe_path = './lib/LAStools/bin/'
    command = ''
    output_filename = ''

    if type(filename) is list:
        output_filename = 'corresponding-lidars_split'
        inputs = ' '.join(f'{config.PATH_LAZ}{config.LIDAR_RUNS[0]}/{f}' for f in filename)
        command = f'{exe_path}lasground -i {inputs} -o {output_filename}.las -odir {data_path} -merged '
    else:
        output_filename = f'{filename}_split'
        command = f'{exe_path}lasground -i {data_path}{filename}.las -o {output_filename}.las -odir {data_path} -merged -not_airborne '   # -odix _split 

    subprocess.call(command)
    p = subprocess.check_output(command)
    print(p)


def plot(las, lib='open3d'):
    if lib == 'open3d':
        geometry_data = []
        for lidar_data in las:
            data = np.column_stack((lidar_data.X, lidar_data.Y, lidar_data.Z))
            pcd_data = create_open3d_pc(data)
            geometry_data.append(pcd_data)
        axis = open3d.geometry.TriangleMesh.create_coordinate_frame(size=0.05, origin=[55565, 0, 27679])
        geometry_data.append(axis)
        open3d.visualization.draw_geometries(geometry_data)
    else:
        # From https://gis.stackexchange.com/questions/277317/visualizing-las-with-matplotlib
        # getting scaling and offset parameters
        las_scaleX = las.header.scale[0]
        las_offsetX = las.header.offset[0]
        las_scaleY = las.header.scale[1]
        las_offsetY = las.header.offset[1]
        las_scaleZ = las.header.scale[2]
        las_offsetZ = las.header.offset[2]

        # calculating coordinates
        p_X = np.array((las.X * las_scaleX) + las_offsetX)
        p_Y = np.array((las.Y * las_scaleY) + las_offsetY)
        p_Z = np.array((las.Z * las_scaleZ) + las_offsetZ)

        # plotting points
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(p_X, p_Y, p_Z, c='r', marker='o')
        plt.show()

