from datetime import datetime


ROOT_PATH_DATA = 'data/'
PATH_VIDEO = f'{ROOT_PATH_DATA}Video/'
PATH_LAZ = f'{ROOT_PATH_DATA}LAZ/'
CAMERA_CALIBRATION_FILE = f'{ ROOT_PATH_DATA }Video/Calibration/3DOneCAM_20180706_201002303.xml'

DXF_APPID = 'GROUND_TRUTH'

TRAIN_SPLIT_SIZE = 0.7
TEST_SPLIT_SIZE = 0.15
VAL_SPLIT_SIZE = 0.15

VIDEO_RUNS_CONFIG = {
    'ep11-201002303-20190430-075921': {
        'min': 2400,    # Min & maximum frame id to select per video run
        'max': 3721,
        # Degenerate cases, flag object to remove it from frame (list) 
        # or remove it entirely (bool) when creating training data
        # ('Small [object]', 'Large [object]' means: needs novel class)
        # We remove most of the smaller markers because their bounding 
        # boxes are too high. Order of bounding boxes are given in 
        # order of camera frames
        'ignore_objects': {
           # bbox#88: ['frame_08888'],      # Reason to ignore object in frame(s)
           # bbox#888: True,                # Reason to ignore object entirely
           # rail#8: True,                  # Reason to ignore object entirely
            
            'bbox#71': True,                # Double marker
            'bbox#72': True,    
            'bbox#73': True,                # Double marker  
            'bbox#74': True,    
            'bbox#77': True,                # Occluded markers
            'bbox#78': True,   
            'bbox#79': True, 
            'bbox#80': True, 
            'bbox#81': True,                # Small marker
            'bbox#82': True,                # Occluded markers
            'bbox#83': True,                
            'bbox#86': True,                # Occluded marker
            'bbox#88': True,                # Double marker
            'bbox#89': True,                # Double marker
            'bbox#91': True,                # Double marker
            'bbox#128': ['frame_03713'],    # Occluded arker
            'bbox#129': True,               # Double marker
            'bbox#131': True,               # Double marker
            'bbox#132': True,               # Double marker
            'bbox#133': True,               # Not a railroad sign
            'bbox#322': ['frame_03702'],    # Cabinet behind signal
        },
        # Degenerate cases, remove all labels from frame
        'ignore_frames': [  
          # 'frame_88888',      # Reason to ignore all labels from frame
            
            'frame_02904',      # Occluded object
        ],
    },
    'ep11-201002303-20190430-080329': {
        'min': 0, 
        'max': 3342,
        'ignore_objects': {
            'bbox#323': True,               # Cabinet behind vegetation
            'bbox#324': True,               # Cabinet behind vegetation
            'bbox#286': True,               # Small marker
            'bbox#12': True,                # Small signal light
            'bbox#139': True,               # Small signal light
            'bbox#142': True,               # Small marker
            'bbox#143': True,               # Small marker
            'bbox#15': True,                # Signal light behind wagon
            'bbox#331': True,               # Cabinet behind wagon
            'bbox#332': True,               # Cabinet behind wagon
            'bbox#18': True,                # Small signal light
            'bbox#146': True,               # Small signal light
            'bbox#147': True,               # Small marker
            'bbox#150': ['frame_00679',     # Occluded marker
                         'frame_00680',
                         'frame_00681'],
            'bbox#154': ['frame_00729',
                         'frame_00730', 
                         'frame_00731', 
                         'frame_00732', 
                         'frame_00733', 
                         'frame_00734', 
                         'frame_00735', 
                         'frame_00736', 
                         'frame_00737', 
                         'frame_00738', 
                         'frame_00739'],    # Marker behind signal light
            'bbox#155': ['frame_00736'],    # Marker not completely within frame
            'bbox#156': True,               # Unclear marker
            'bbox#157': True,               # Unclear marker
            'bbox#19': True,                # Small signal light
            'bbox#160': True,               # Double marker
            'bbox#161': True,              
            'bbox#162': True,               # Double marker             
            'bbox#163': True,                   
            'bbox#166': True,               # Far away marker  
            'bbox#168': True,               # Not a railroad sign (needs novel class)
            'bbox#339': True,               # Far away cabinet
            'bbox#338': True,               # Occluded cabinet
            'bbox#28': True,                # Not a railroad signal light
            'bbox#350': True,               # Far away cabinet
            'bbox#341': True,               # Occluded cabinet
            'bbox#340': True,               # Occluded cabinet
            'bbox#172': True,               # Occluded marker
            'bbox#173': True,               # Occluded marker
            'bbox#342': True,               # Occluded cabinet
            'bbox#344': True,               # Large cabinet
            'bbox#345': True,               # Small cabinet
            'bbox#346': ['frame_02422', 
                         'frame_02423'],    # Cabinet behind marker
            'bbox#176': True,               # Small marker
            'bbox#177': True,               # Small marker
            'bbox#348': True,               # Large cabinet (needs novel class)
            'bbox#349': True,               # Occluded cabinet
            'bbox#362': True,               # Large cabinet
            'bbox#355': True,               # Occluded cabinet + incorrect bounding box
            'bbox#181': True,               # Occluded marker
            'bbox#179': True,               # Small marker
            'bbox#180': True,               # Small marker
            'bbox#184': True,               # Occluded marker
            'bbox#185': True,               # Small marker
            'bbox#186': True,               # Small marker
            'bbox#187': True,               # Small marker
            'bbox#190': True,               # Small marker
            'bbox#191': True,               # Not a railroad sign
        },
        'ignore_frames': [],
    },
    'ep11-201002303-20190430-080712': {
        'min': 0,        
        'max': 2922,
        'ignore_objects': {
            'bbox#367': ['frame_00057',
                         'frame_00058',
                         'frame_00059',
                         'frame_00060',
                         'frame_00061',
                         'frame_00062',
                         'frame_00063',
                         'frame_03064'],    # Small cabinet 
            'bbox#192': True,               # Small marker
            'bbox#195': True,               # Small marker
            'bbox#371': True,               # Large cabinet
            'bbox#372': True,               # Not a railroad cabinet
            'bbox#374': True,               # Large cabinet
            'bbox#287': True,               # Double marker
            'bbox#197': True,               # Ambiguous marker
            'bbox#33': True,                # Not a railroad signal light
            'bbox#34': True,                # Not a railroad signal light
            'bbox#35': True,                # Not a railroad signal light
            'bbox#38': True,                # Not a railroad signal light
            'bbox#41': True,                # Ambiguous signal light
            'bbox#205': True,               # Ambiguous marker
            'bbox#210': True,               # Ambiguous marker
            'bbox#386': True,               # Occluded cabinet
            'bbox#387': True,               # Occluded cabinet
            'bbox#388': True,               # Large cabinet
            'bbox#395': True,               # Occluded cabinet
            'bbox#396': True,               # Large cabinet
            'bbox#215': True,               # Small marker
            'bbox#217': True,               # Double marker
            'bbox#218': True,               # Double marker
            'bbox#220': True,               # Not a railroad sign
            'bbox#221': True,               # Not a railroad sign
            'bbox#42': True,                # Not a railroad signal light
            'bbox#43': True,                # Not a railroad signal light
            'bbox#44': True,                # Not a railroad signal light
            'bbox#45': True,                # Not a railroad signal light
            'bbox#398': True,               # Occluded cabinet
            'bbox#399': True,               # Large cabinet
            'bbox#288': True,               # Small marker
            'bbox#248': True,               # Small marker
            'bbox#427': True,               # Occluded cabinet
            'bbox#250': True,               # Double marker
            'bbox#251': True, 
            'bbox#253': True,               # Double marker
            'bbox#255': True,               # Far away small marker
            'bbox#257': True,               # Small marker
            'bbox#259': True,               # Small marker
            'bbox#282': True,               # Marker behind vegetation
            'bbox#289': True,               # Marker behind wagon
            'bbox#442': True,               # Cabinet behind wagon
            'bbox#276': True,               # Marker behind wagon
            'bbox#267': True,               # Double marker
            'bbox#269': True,               # Small marker
            'bbox#270': True,               # Small marker
            'bbox#436': True,               # Occluded cabinet
            'bbox#437': True,               # Large cabinet
            'bbox#64': True,                # Not a railroad signal light
            'bbox#65': True,                # Not a railroad signal light
            'bbox#66': True,                # Not a railroad signal light
            'bbox#67': True,                # Not a railroad signal light
            'bbox#296': True,               # Large cabinet
            'bbox#271': True,               # Ambiguous marker
            'bbox#273': True,               # Double marker
            'bbox#274': True,               # Small marker
            'bbox#275': True,               # Small marker
            'bbox#278': True,               # Ambiguous marker
        },
        'ignore_frames': [],
    },
}

LIDAR_RUNS = ['2019-04-30_b_9', '2019-04-30_b_10']
DATE_DATA = datetime(2019, 4, 30)
CAMS = ['cam1', 'cam2', 'cam3']
CONFIG_BBOXES = {   	        # Configure bounding box around object type
    'SU-TR-Rails-Z': {
        'name': 'Rails',
        'height': 1,
        'resize_factor': 1,  
        'color': (255, 0, 255),
        'color_text': (0, 0, 0),
    },
    'TL-TL-Signal light-Z': {
        'name': 'Signal_light',
        'height': 5,
        'resize_factor': 20,
        'color': (0, 255, 0),
        'color_text': (0, 0, 0),
    },
    'SU-TR-Markers-Z': {
        'name': 'Marker',       # Signs
        'height': 2.5,
        'resize_factor': 15,    # DWG circle is smaller than bbox, so add resize factor
        'color': (255, 0, 0),
        'color_text': (0, 0, 0),
    },
    'SU-UT-Cabinet-Z': { 
        'name': 'Cabinet',
        'height': 2,
        'resize_factor': 5,
        'color': (246, 190, 0),
        'color_text': (0, 0, 0),
    },
}

Z_CORRECTION = 0
# Z_CORRECTION = -0.020
CLOSEBY_DISTANCE = 3
FARAWAY_DISTANCE = 30

LASPY_FILE_VERSION = '1.4'
LASPY_POINT_FORMAT = 6


def get_video_metadata_path(video_run_name, camera_name):
    return f'{ ROOT_PATH_DATA }Video/externalOrientation/{ video_run_name }/{ camera_name }.mjpg_EO_InScope.csv'
